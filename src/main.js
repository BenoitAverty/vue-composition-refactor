import { createApp } from 'vue'
import App from './App.vue'


// Devtools for vue 3 : https://github.com/vuejs/vue-devtools/issues/1244#issuecomment-691561843

createApp(App).mount('#app')
